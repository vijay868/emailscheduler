﻿using common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmailScheduler
{
    class Program
    {
        static System.Timers.Timer timer;
        static private DatabaseObject dbo;
        static private EmailProcessor processor;
        static private Scheduler scheduler;
        static void Main(string[] args)
        {
            var str = ConfigurationManager.ConnectionStrings["dbString"].ConnectionString;

            dbo = new DatabaseObject(str);
            processor = new EmailProcessor();
            scheduler = new Scheduler(dbo, processor);

            timer = new System.Timers.Timer();
            timer.Interval = 1000 * 60 * 60 * 24;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            timer.Start();
        }

        static void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Task.Run(async () =>
            {
                await scheduler.ProcessEmails();
            });            
        }
    }

    public class Scheduler
    {
        private readonly DatabaseObject dbo;
        private readonly EmailProcessor processor;
        public Scheduler(DatabaseObject dbo, EmailProcessor processor)
        {
            this.dbo = dbo;
            this.processor = processor;
        }
        public async Task ProcessEmails()
        {
            
            var emails = await dbo.GetMailsForProcessing();
            emails.ForEach(mail =>
            {
                processor.SentEmail(mail.fEmail, $"<webservice>/{mail.fId}").ConfigureAwait(false);
                dbo.updatesentemailcount(mail.fEmail).ConfigureAwait(false);
            });
        }
    }    
}
