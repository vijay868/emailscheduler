﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using common;

namespace EmailService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        [HttpGet, Route("{fid}")]
        public async Task<IActionResult> Index(int fid)
        {
            var dbo = new DatabaseObject("<database connection string>");
            var emailProcessor = new EmailProcessor();

            var emailObj = await dbo.GetDetailsByFid(fid);            
            var emailTask = emailProcessor.SentEmail(emailObj.fEmail, "Thank you");
            var updateTask = dbo.updateemailopenedstatus(fid);
            Task.WaitAll(emailTask, updateTask);
            return Ok();
        }
    }
}