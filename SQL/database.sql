create database Jobs;

go

IF OBJECT_ID('dbo.Emails','U') IS NULL
CREATE TABLE dbo.Emails(
	 fId int identity(1,1),
	 fEmail nvarchar(200),
	 fIsEmailOpened bit,
	 fNoOfMailSent int,
	 CONSTRAINT fId PRIMARY KEY CLUSTERED(
	 fId ASC
)
)
GO

create or alter proc usp_getmails
as
begin
 select
      fid,
	  femail,
	  fIsEmailOpened,
	  fNoOfMailSent
 from emails
 where fIsEmailOpened = cast(0 as bit) And fNoOfMailSent < 3
end

go

create or alter proc usp_updatemailsentcout(@email nvarchar(200))
as
begin
 declare @sentcount int = 0;
 set @sentcount = (select count(1) from Emails where fEmail = @email);
 if @sentcount < 3
  begin
	set @sentcount = @sentcount + 1;
	update emails set fNoOfMailSent = @sentcount where fEmail = @email;
  end
end

go

create or alter proc usp_updateisemailopenedstatus(@fid int)
as
begin
 update Emails set fIsEmailOpened = cast(1 as bit) where fid = @fid;
end

go

create or alter proc usp_getdetailsbyfid(@fid int)
as
begin
 select
      fid,
	  femail,
	  fIsEmailOpened,
	  fNoOfMailSent
 from emails
 where fid = @fid
end