﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common
{
    public class Emails
    {
        public int fId { get; set; }

        public string fEmail { get; set; }

        public bool fIsEmailOpened { get; set; }

        public int fNoOfMailSent { get; set; }
    }
}
