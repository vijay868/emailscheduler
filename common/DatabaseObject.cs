﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common
{
    public class DatabaseObject
    {
        private readonly string connectionString;
        public DatabaseObject(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public async Task<List<Emails>> GetMailsForProcessing()
        {
            List<Emails> emails = new List<Emails>();
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "usp_getmails";

                SqlDataReader reader = cmd.ExecuteReader();
                var _email = reader.GetOrdinal("femail");
                var fid = reader.GetOrdinal("fid");
                var fIsEmailOpened = reader.GetOrdinal("fIsEmailOpened");
                var fNoOfMailSent = reader.GetOrdinal("fNoOfMailSent");
                Emails email = null;
                while (await reader.ReadAsync())
                {
                    email = new Emails
                    {
                        fId = reader.GetInt32(fid),
                        fEmail = reader.GetString(_email),
                        fIsEmailOpened = reader.GetBoolean(fIsEmailOpened),
                        fNoOfMailSent = reader.GetInt32(fNoOfMailSent)
                    };

                    emails.Add(email);
                }

                conn.Close();

                return emails;
            }
        }

        public async Task updatesentemailcount(string email)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "usp_updatemailsentcout";

                cmd.Parameters.AddWithValue("@email", email);

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task updateemailopenedstatus(int fid)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "usp_updatemailsentcout";

                cmd.Parameters.AddWithValue("@fId", fid);

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<Emails> GetDetailsByFid(int fid)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "usp_getdetailsbyfid";

                SqlDataReader reader = cmd.ExecuteReader();
                var _email = reader.GetOrdinal("femail");
                var _fid = reader.GetOrdinal("fid");
                var fIsEmailOpened = reader.GetOrdinal("fIsEmailOpened");
                var fNoOfMailSent = reader.GetOrdinal("fNoOfMailSent");
                Emails email = null;
                while (await reader.ReadAsync())
                {
                    email = new Emails
                    {
                        fId = reader.GetInt32(fid),
                        fEmail = reader.GetString(_email),
                        fIsEmailOpened = reader.GetBoolean(fIsEmailOpened),
                        fNoOfMailSent = reader.GetInt32(fNoOfMailSent)
                    };
                }

                conn.Close();

                return email;
            }
        }
    }
}
