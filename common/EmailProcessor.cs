﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace common
{
    public class EmailProcessor
    {
        public async Task SentEmail(string toAddress, string body)
        {
            MailMessage mail = new MailMessage();
            using (var client = new SmtpClient())
            {
                mail.To.Add(toAddress);
                mail.IsBodyHtml = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                mail.Subject = "Mail Scheduler";
                mail.Body = body;

                await client.SendMailAsync(mail);
            }
        }
    }
}
